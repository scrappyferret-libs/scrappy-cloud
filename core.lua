--- Required libraries.
local json = require( "json" )

-- Localised functions.
local time = os.time
local open = io.open
local close = io.close
local pathForFile = system.pathForFile
local encode = json.encode
local decode = json.decode
local upload = network.upload
local download = network.download
local remove = os.remove
local gsub = string.gsub

-- Localised values.

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Get the params, if any
	self._params = params or {}

	-- Directory for writing data to before uploads
	self._baseDir = system.TemporaryDirectory

	-- Set the api url if required
	self:setAPIUrl( self._params.apiUrl )

end


-- Sets the url for the external API that data is submitted to.
-- @param url A table containing an 'upload' and 'download' url.
function library:setAPIUrl( url )
	self._apiURL = url
end

--- Gets the api url, if set.
-- @return The url.
function library:getAPIUrl()
	return self._apiURL
end

--- Uploads some data.
-- @param data The data to upload.
-- @param onComplete The onComplete handler for the upload.
-- @param onProgress The onProgress handler for the upload. Optional.
function library:upload( data, listener, onProgress )

	local onComplete = function( id )
		native.setActivityIndicator( false )
		if listener and type( listener ) == "function" then listener( id ) end
	end

	native.setActivityIndicator( true )

	-- Write out the data to a file and upload it
	self:_upload( self:_write( data ), onComplete, onProgress )

end

--- Downloads some data.
-- @param id The id of the exported data.
-- @param listener The listener for the download.
-- @param onProgress The onProgress handler for the download. Optional.
function library:download( id, listener, onProgress )

	if id then

		-- Trim any whitespace
		id = gsub( id, "%s+", "" )

		local onComplete = function( data )
			native.setActivityIndicator( false )
			if listener and type( listener ) == "function" then listener( decode( data ) ) end
		end
		native.setActivityIndicator( true )
		self:_download( id, onComplete, onProgress )

	end

end

--- Writes some data to a file.
-- @param data The data to upload.
function library:_write( data )

	-- Do we have an error?
	if data then

		-- Generate a filename for the local error file
		local filename = time() .. ".cloud"

		-- Create the path
		local path = pathForFile( filename, self._baseDir )

		-- Open the file for appending
		local file, errorString = open( path, "w" )

		-- Do we have a file handle?
		if file then

			-- Andcode the error and write it to the file
		   file:write( encode( data ) )

		   -- Close the file handle
		   close( file )

		else

			-- Error occurred; output the cause
			print( "File error: " .. errorString )

		end

		-- Nil out the handle
		file = nil

		-- Return the filename
		return filename

	end

end

--- Uploads a file to the server.
-- @param filename Filename of the file to upload.
-- @param onComplete The onComplete handler for the upload.
-- @param onProgress The onProgress handler for the upload. Optional.
function library:_upload( filename, onComplete, onProgress )

	-- Do we have an api url?
	if self._apiURL then

		local bytesTransferred = 0

		-- Generate the path to the file
		local path = pathForFile( filename, self._baseDir )

		-- Listener for network calls
		local function networkListener( event )

			-- Was there an error? ( Oh the ironing! )
			if event.isError then
				print( "Network error: ", event.response )

				if onComplete and type( onComplete ) == "function" then onComplete( event.response, event.isError ) end

			else

				if event.phase == "ended" then

					-- Was the response a 0 i.e. it broke?
					if event.response == "0" then


					else -- Otherwise, get the cloud name and delete the local file

						-- Delete the crash file as it was submitted successfully
						remove( path )

					end

					if onComplete and type( onComplete ) == "function" then onComplete( event.response ) end

				elseif event.phase == "progress" then

					if onProgress then

						bytesTransferred = bytesTransferred + ( event.bytesTransferred or 0 )

						if event.bytesEstimated > 0 then
							bytesEstimated = ( bytesEstimated or 0 ) + event.bytesEstimated
						end

						onProgress( bytesTransferred, estimatedBytes )

					end

				end

			end

		end

		-- Finally upload it
		upload(
			self._apiURL.upload,
			"POST",
			networkListener,
			{ progress = true },
			filename,
			self._baseDir,
			"application/json"
		)

	end

end


--- Downloads some data from the server.
-- @param id The id of the data.
-- @param onComplete The onComplete handler for the download.
-- @param onProgress The onProgress handler for the download. Optional.
function library:_download( id, onComplete, onProgress )

	local bytesTransferred = 0

	local function networkListener( event )
		if event.phase == "ended" then
			if onComplete and type( onComplete ) == "function" then onComplete( event.response ) end
		elseif event.phase == "progress" then
			if onProgress then
				bytesTransferred = bytesTransferred + ( event.bytesTransferred or 0 )

				if event.bytesEstimated > 0 then
					bytesEstimated = ( bytesEstimated or 0 ) + event.bytesEstimated
				end

				onProgress( bytesTransferred, estimatedBytes )
			end
		end
	end

	local params = {}
	params.progress = "download"

	local url = self._apiURL.download .. "?id=" .. id

	network.request( url, "GET", networkListener, params )

end

--- Destroys the time libray.
function library:destroy()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Cloud library
if not Scrappy.Cloud then

	-- Then store the library out
	Scrappy.Cloud = library

end

-- Return the new library
return library

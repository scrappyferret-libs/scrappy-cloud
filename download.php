<?php

    /*

     download.php - basic download module for Scrappy Cloud
     no warranty implied; use at your own risk

     author: Graham Ranson (graham@scrappyferret.com)
     url: scrappyferret.com

     NOTES

     I really dislike PHP, databases, and web dev in general so please don't hate me for how terrible this is.

     LICENSE

     See end of file for license information.

    */

    // Include database config with $servername, $username, $password, and $dbname all set
    include 'config.php';

    // Create connection
    $conn = new mysqli( $servername, $username, $password, $dbname );

    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    // Do we have a passed in id?
    if ( isset( $_GET[ "id" ] ) & $_GET[ "id" ] != "" ) {

        // Get the passed in ID
        $name = $_GET[ "id" ];

        // Prepare the statement
        $stmt = $conn->prepare( "SELECT data FROM cloud WHERE name = ?" );

        // Ensure we have a statement
        if ( $stmt != false ) {

            // Bind the paramater
            $stmt->bind_param( "s", $name );

            // Execute the statement
            $stmt->execute();

            // Bind the result
            $stmt->bind_result( $data );

            // Fetch the data
            while( $stmt->fetch() ) {

                // Return the data
                echo $data;
            }

            // Close out the statement
            $stmt->close();

        }
    }

    // Close the connection
    $conn->close();

?>

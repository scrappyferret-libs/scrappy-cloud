<?php

    /*

     upload.php - basic upload module for Scrappy Cloud
     no warranty implied; use at your own risk

     author: Graham Ranson (graham@scrappyferret.com)
     url: scrappyferret.com

     NOTES

     I really dislike PHP, databases, and web dev in general so please don't hate me for how terrible this is.

     LICENSE

     See end of file for license information.

    */

    // Include database config with $servername, $username, $password, and $dbname all set
    include 'config.php';

    // Create connection
    $conn = new mysqli( $servername, $username, $password, $dbname );

    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error );
    }

    // Get the passed in data
    $data = file_get_contents( 'php://input' );

    // Function to generate a random name
    function generateRandomString( $length = 10, $validChars = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ" ) {
        return substr( str_shuffle( str_repeat ( $x = $validChars, ceil( $length / strlen( $x ) ) ) ), 1, $length );
    }

    // Generate a name
    $name = generateRandomString();

    // Function to check if the name exists
    function nameExists( $conn, $name ) {

        // Prepare the statement
        $stmt = $conn->prepare( "SELECT name FROM cloud WHERE name = ?" );

        // Ensure we have a statement
        if ( $stmt != false ) {

            // Bind the paramater
            $stmt->bind_param( "s", $name );

            // Execute the statement
            $stmt->execute();

            // Store the result
            $stmt->store_result();

            // Close out the statement
            $stmt->close();

            // Return if we already have this name
            return $stmt->num_rows > 0;
        }

    }

    // While the name exists
    while( nameExists( $conn, $name ) )
    {
        // Generate a new one
        $name = generateRandomString();
    }

    // Prepare the sql statement
    $stmt = $conn->prepare('INSERT INTO cloud ( name, data ) VALUES( ?, ? )');

    // Ensure we have a statement
    if ( $stmt != false ) {

        // Bind the paramaters
        $stmt->bind_param( "ss", $name, $data );

        // Execute the statement
        $stmt->execute();

        // Did we fail to affect anyone?
        if( $stmt->affected_rows === 0 ) {

            // Return that it failed
            echo 0;

        }
        else {

            // Return the name
            echo $name;
        }

        // Close out the statement
        $stmt->close();
    }
    else
    {
        // Return that it failed
        echo 0;
    }

    // Close the connection
    $conn->close();

    /*
    MIT License

    Copyright (c) 2021 Scrappy Ferret Ltd

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
    /*

?>
